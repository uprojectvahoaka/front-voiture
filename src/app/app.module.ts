import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AccueilModule } from './pages/accueil/accueil.module';
import { ContainerModule } from './racine/container/container.module';
import { GuiModule } from './racine/gui/gui.module';
import { HeroesComponent } from './pages/heroes/heroes.component';
import { ParticlesModule } from 'angular-particle';
import { DataTablesModule } from 'angular-datatables';
import { ProjetModule } from './pages/projet/projet.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent
  ],
  imports: [
    BrowserModule,
    DataTablesModule,
    FormsModule,
    AppRoutingModule,
    ContainerModule,
    AccueilModule,
    ProjetModule,
    GuiModule,
    HttpClientModule,
    ParticlesModule
  ],
  providers: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
