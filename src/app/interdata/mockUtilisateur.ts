import { Utilisateur } from "../interfaces/utilisateur";

export const UTILISATEUR: Utilisateur[] = [
    {
        id: 1,
        nom: 'nom1',
        email: 'nom1@gmail.com',
        password: 'password1',
        photo: 'lienPhoto1',
        passwd: 'passwd1',
        surname: 'surname1',
        etat: 'etat1',
        sexe: 'male'
    },    
    {
        id: 2,
        nom: 'nom2',
        email: 'nom2@gmail.com',
        password: 'password2',
        photo: 'lienPhoto2',
        passwd: 'passwd2',
        surname: 'surname2',
        etat: 'etat2',
        sexe: 'female'
    }
];