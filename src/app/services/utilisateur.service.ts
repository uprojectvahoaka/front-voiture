import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Utilisateur } from '../interfaces/utilisateur';

@Injectable({
  providedIn: 'root',
})
export class UtilisateurService {

  // Configuration API  ,  LIEN
  // "http://localhost:3000" |'http://mailbusiness.fr'|'http://api.evertuoo.devforge.eu'|'http://192.168.43.51:3000'
  apiURL = 'http://localhost:8080';

  constructor(private http: HttpClient) {}

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    }),
  };

  // HttpClient API put() method => Update Utilisateur
  // http://localhost:8080/rproject-services/authentication/seheno 1/seheno
  loginUtilisateur(Utilisateur): Observable<Utilisateur> {
    return this.http
      .get<Utilisateur>(
        this.apiURL + '/rproject-services/authentication/'+Utilisateur.nom+'/'+Utilisateur.password,
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  // // HttpClient API get() method => Fetch Utilisateur
  // getUtilisateur(id): Observable<Utilisateur> {
  //   return this.http
  //     .get<Utilisateur>(this.apiURL + '/Utilisateurs/' + id, this.httpOptions)
  //     .pipe(retry(1), catchError(this.handleError));
  // }

  // // HttpClient API post() method => Create Utilisateur
  // createUtilisateur(Utilisateur): Observable<Utilisateur> {
  //   return this.http
  //     .post<Utilisateur>(
  //       this.apiURL + '/Utilisateurs',
  //       JSON.stringify(Utilisateur),
  //       this.httpOptions
  //     )
  //     .pipe(retry(1), catchError(this.handleError));
  // }

  // // HttpClient API put() method => Update Utilisateur
  // updateUtilisateur(id, Utilisateur): Observable<Utilisateur> {
  //   return this.http
  //     .put<Utilisateur>(
  //       this.apiURL + '/Utilisateurs/' + id,
  //       JSON.stringify(Utilisateur),
  //       this.httpOptions
  //     )
  //     .pipe(retry(1), catchError(this.handleError));
  // }

  // // HttpClient API delete() method => Delete Utilisateur
  // deleteUtilisateur(id) {
  //   return this.http
  //     .delete<Utilisateur>(this.apiURL + '/Utilisateurs/' + id, this.httpOptions)
  //     .pipe(retry(1), catchError(this.handleError));
  // }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(['Erreur HTTP', errorMessage]);
    return throwError(errorMessage);
  }
}
