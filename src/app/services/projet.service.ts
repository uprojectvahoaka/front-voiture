
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Projet } from '../interfaces/projet';

@Injectable({
  providedIn: 'root',
})
export class ProjetService {

  // Configuration API  ,  LIEN
  // "http://localhost:3000" |'http://mailbusiness.fr'|'http://api.evertuoo.devforge.eu'|'http://192.168.43.51:3000'
  apiURL = 'http://localhost:8080';

  constructor(private http: HttpClient) {}

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    }),
  };


  // HttpClient API get() method => Fetch Projets list
  // http://localhost:8080/rproject-services/voitures
  getAllVoitture(): Observable<Projet> {
    return this.http
      .get<Projet>(this.apiURL + '/rproject-services/voitures', this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }



   // http://localhost:8080/rproject-services/createCommentaire/test224/1/1
   getCreateCommentaire(Commentaire): Observable<Projet> {
    return this.http
      .get<Projet>(this.apiURL + '/rproject-services/createCommentaire/'+Commentaire.message+'/'+Commentaire.userId+'/'+Commentaire.voitureId, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }



  // http://localhost:8080/rproject-services/voitures/1/commentaires
  getCommentaireByVoiture(voitureId): Observable<Projet> {
    return this.http
      .get<Projet>(this.apiURL + '/rproject-services/voitures/'+voitureId+'/commentaires', this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }


  // HttpClient API get() method => Fetch Projets list
  getProjets(): Observable<Projet> {
    return this.http
      .get<Projet>(this.apiURL + '/projets', this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }


 

  // // HttpClient API get() method => Fetch Projet
  // getProjet(id): Observable<Projet> {
  //   return this.http
  //     .get<Projet>(this.apiURL + '/Projets/' + id, this.httpOptions)
  //     .pipe(retry(1), catchError(this.handleError));
  // }

  // // HttpClient API post() method => Create Projet
  // createProjet(Projet): Observable<Projet> {
  //   return this.http
  //     .post<Projet>(
  //       this.apiURL + '/Projets',
  //       JSON.stringify(Projet),
  //       this.httpOptions
  //     )
  //     .pipe(retry(1), catchError(this.handleError));
  // }

  // // HttpClient API put() method => Update Projet
  // updateProjet(id, Projet): Observable<Projet> {
  //   return this.http
  //     .put<Projet>(
  //       this.apiURL + '/Projets/' + id,
  //       JSON.stringify(Projet),
  //       this.httpOptions
  //     )
  //     .pipe(retry(1), catchError(this.handleError));
  // }

  // // HttpClient API delete() method => Delete Projet
  // deleteProjet(id) {
  //   return this.http
  //     .delete<Projet>(this.apiURL + '/Projets/' + id, this.httpOptions)
  //     .pipe(retry(1), catchError(this.handleError));
  // }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(['Erreur HTTP', errorMessage]);
    return throwError(errorMessage);
  }
}
