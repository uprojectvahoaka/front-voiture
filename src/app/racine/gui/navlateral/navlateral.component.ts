import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navlateral',
  templateUrl: './navlateral.component.html',
  styleUrls: ['./navlateral.component.css']
})
export class NavlateralComponent implements OnInit {

  @Output() changeContainer: EventEmitter<any> = new EventEmitter();

  /* ################################  VARIABLE  #################################*/
  public containerName: string;

  /* ################################  CONSTRUCOR  #################################*/
  constructor() { 
    this.containerName = "app-accueil";
   }

  ngOnInit() {
  }

  /* ################################  FONCTION  #################################*/
  public proc_change_container(){
    this.changeContainer.emit(this.containerName);
  }

  public changeRouting(nameRoute: any){
    this.containerName = nameRoute;
    this.proc_change_container();
  }

 

  public changeUtilisateur(){
    this.containerName = "app-liste-utilisateur";
    this.proc_change_container();
  }

  public changeAide(){
    this.containerName = "#";
  }

}
