import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-navheader',
  templateUrl: './navheader.component.html',
  styleUrls: ['./navheader.component.css']
})
export class NavheaderComponent implements OnInit {

  @Output() hideMenu: EventEmitter<any> = new EventEmitter();

  @Output() showMenu: EventEmitter<any> = new EventEmitter();

  @Output() toggleParam: EventEmitter<any> = new EventEmitter();

  /* ################################  VARIABLE  #################################*/

  /* ################################  CONSTRUCTOR  #################################*/
  constructor(
  	private router: Router
  ) { }

  ngOnInit() {
  }

  

  /* ################################  FONCTIONS  #################################*/
  /**
   * Authentification de l'utilisateur
   */
  public proc_deconnexion() {
    // Accès à la page d'acceuil
    this.router.navigate(["login"]);
  }

  /**
   * Toogle parametre Onglet avec animation 
   */
  public proc_toggle_param() {
    this.toggleParam.emit(null);
  }


  public proc_emitevent_hide(){
    this.hideMenu.emit(null);
  }

  public proc_emitevent_show(){
    this.showMenu.emit(null);
  }

}
