import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavheaderComponent } from './navheader/navheader.component';
import { NavlateralComponent } from './navlateral/navlateral.component';
import { NavfooterComponent } from './navfooter/navfooter.component';
import { ProjetModule } from 'src/app/pages/projet/projet.module';

@NgModule({
  imports: [
    CommonModule,
    ProjetModule
  ],
  declarations: [
 	NavheaderComponent,
 	NavlateralComponent,
	NavfooterComponent
  ],
  exports: [
 	NavheaderComponent,
 	NavlateralComponent,
	NavfooterComponent
  ]
})
export class GuiModule { }
