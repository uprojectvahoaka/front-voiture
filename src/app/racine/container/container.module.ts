import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Container1Component } from './container1/container1.component';
import { GuiModule } from '../gui/gui.module';
import { AccueilModule } from 'src/app/pages/accueil/accueil.module';
import { UtilisateurModule } from 'src/app/pages/utilisateur/utilisateur.module';
import { ProjetModule } from 'src/app/pages/projet/projet.module';

@NgModule({
  exports: [Container1Component],
  declarations: [Container1Component],
  imports: [
    CommonModule,
    AccueilModule,
    UtilisateurModule,
    ProjetModule,
    GuiModule
  ]
})
export class ContainerModule { }
