import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import * as $ from 'jquery';

@Component({
  selector: 'app-container1',
  templateUrl: './container1.component.html',
  styleUrls: ['./container1.component.css']
})
export class Container1Component implements OnInit {

  /* ##########################           VARIABLE           #################################### */
  public visibleLeftMenu: boolean;
  public visibleRightParam: boolean;
  public contentClassValue: number;
  public ecranValue: string;

  /* ##########################        CONSTRUCTEUR          #################################### */
  constructor(private router: Router) { 

    // Configuration d'initialisation 
    this.visibleLeftMenu = true;
    this.visibleRightParam = false;
    this.contentClassValue = 2;

    // Initialisation de variable d'accueil
    this.ecranValue = "app-accueil";

  }

  ngOnInit() {

    console.log(".......  "+this.ecranValue);

  }

  /* ##########################           FONCTION           #################################### */

  /**
   * Authentification de l'utilisateur
   */
  public proc_deconnexion() {
    // Accès à la page de connexion
    this.router.navigate(["login"]);
  }

  public hideMenuLateral(value) {
    this.visibleLeftMenu = false;
  }

  public showMenuLateral(value) {
    this.visibleLeftMenu = true;
  }

  public ToggleMenu() {
    if ( this.visibleLeftMenu === false ) {

      this.visibleLeftMenu = true;
      if(this.visibleRightParam == false){
        this.contentClassValue = 1;
      }else{
        this.contentClassValue = 0;
      }

    } else if ( this.visibleLeftMenu === true ) {

      this.visibleLeftMenu = false;
      if(this.visibleRightParam == false){
        this.contentClassValue = 3;
      }else{
        this.contentClassValue = 2;
      }
    } else {
      // Todo
    }
  }

  public ToggleMenuParam(value) {

    // 0: LR , 1: !LR, 2: L!R, 3: !L!R  
    if ( this.visibleRightParam == false) {

      this.visibleRightParam = true;
      if(this.visibleLeftMenu == false){
        this.contentClassValue = 1;
      }else{
        this.contentClassValue = 0;
      }
    }  else if ( this.visibleRightParam == true ) {

      this.visibleRightParam = false;
      if(this.visibleLeftMenu == false){
        this.contentClassValue = 3;
      }else{
        this.contentClassValue = 2;
      }
    }   else {
      // OTHER CASE 
    }
    this.showValue();
  }

  public showValue() {
    // console.log( this.visibleLeftMenu +" "+ this.visibleRightParam + " "+ this.contentClassValue );
  }
  
  /**
   * Changement du fenetre de travail au milieu
   * @param value 
   */
  public changeContainer(value: string) {
    this.ecranValue = value;
  }

}
