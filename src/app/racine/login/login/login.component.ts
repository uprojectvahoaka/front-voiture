import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UtilisateurService } from "src/app/services/utilisateur.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  /* ################################  VARIABLES  #################################*/
  myStyle: object = {};
  myParams: object = {};
  width: number = 40;
  height: number = 0;

  public email: string;
  public password: string;

  /* ###############################  CONSTRUCTEUR  ###############################*/
  constructor(
    private router: Router,
    private utilisateurApi: UtilisateurService) {}

  // Configuration OnInit
  ngOnInit() {
    this.email = "";
    this.password = "";

    // Configuration variable
    this.myStyle = {
      position: "fixed",
      width: "100%",
      height: "0%",
      "z-index": -1,
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    };

    this.myParams = {
      particles: {
        number: { value: 200 },
        color: { value: "#ff0000" },
        shape: { type: "triangle" },
      },
    };

    // ###################               Variable de navigateur         ####################### //
    //Configuration des variables gerer on Localstorage
    localStorage.setItem("actual-page", "app-login");

    //Configuration des variables gerer on Sessionstorage
  }

  /* ################################  FONCTIONS  #################################*/
  /**
   * Authentification de l'utilisateur
   */
  public proc_login() {
    // Accès à la page d'acceuil

    console.log([this.email, this.password]);

    let utilisateur = {nom: this.email, password: this.password };
    console.log(utilisateur);

    this.utilisateurApi.loginUtilisateur(utilisateur).subscribe((resultConnect: any)=> {
      console.log(resultConnect);
      if(resultConnect.length > 0) {

        localStorage.setItem("actual-page", "app-accueil");
        localStorage.setItem("visit", "edit");
        localStorage.setItem("userConnected", JSON.stringify(resultConnect[0]));
        let user = resultConnect[0];
        // Constructeur
        localStorage.setItem("userId", user[0]);
        console.log(resultConnect[0]);

        setTimeout(() => {
          this.router.navigate(["accueil"]);
        }, 500);
        
      } else {
        console.log("Wrong password ... ");
      }
    });

    
  }


  public proc_route_acueil(){
    this.router.navigate(["accueil"]);
    localStorage.setItem("visit", "read");
  }

}
