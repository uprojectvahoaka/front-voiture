import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ProjetService } from 'src/app/services/projet.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  /* ################################  VARIABLES  #################################*/
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 20;

  listeVoiture: any[] = [];
  listeImageVoiture = ["voiture1.PNG", "voiture2.PNG", "voiture3.PNG"];

  public commentaire: string;
  public voitureId;
  public userId;
  public listCommentaireVoiture: any[];
  public visit;

  /* ###############################  CONSTRUCTEUR  ###############################*/
  constructor(private router: Router, private projetService : ProjetService) { }

  ngOnInit() {
    this.myStyle = {
      'position': 'fixed',
      'width': '100%',
      'height': '100%',
      'z-index': -1,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
  };

    this.myParams = {
          particles: {
              number: {
                  value: 200,
              },
              color: {
                  value: '#ffff00'
              },
              shape: {
                  type: 'triangle',
              },
      }
    };

    this.projetService.getAllVoitture().subscribe((resData : any) => {
      this.listeVoiture = resData._embedded.voitures;  
    })

    setTimeout(() => {
      this.visit = localStorage.getItem("visit");
    }, 2000);

  }


  /* ################################  FONCTIONS  #################################*/
  /**
   * Authentification de l'utilisateur
   */
  public proc_deconnexion() {
    // Accès à la page d'acceuil
    this.router.navigate(["accueil"]);
  }


  public proc_send_commentaire(paramID) {
    this.userId = localStorage.getItem("userId");
    console.log([this.commentaire, this.userId, this.voitureId]);
    let comment = {message: this.commentaire, userId: this.userId, voitureId: this.voitureId}
     this.projetService.getCreateCommentaire(comment).subscribe( (resCreate: any) => {
       console.log(resCreate);
       this.commentaire = "";
     } );
  }



  public procShowCommentaires(voitureId) {
    this.projetService.getCommentaireByVoiture(voitureId).subscribe( (resCom: any) => {
      console.log(resCom);
      this.listCommentaireVoiture = resCom._embedded.commentaires;
      localStorage.setItem("voitureId", voitureId);
      this.voitureId = voitureId;
    } );
  }


  public procShowFormulaire(voitureId) {
    this.projetService.getCommentaireByVoiture(voitureId).subscribe( (resCom: any) => {
      console.log(resCom);
      this.listCommentaireVoiture = resCom._embedded.commentaires;
      this.voitureId = voitureId;
    } );
    localStorage.setItem("voitureId", voitureId);
    this.voitureId = voitureId;
  }

}
