import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccueilComponent } from './accueil.component';
import { GuiModule } from '../../racine/gui/gui.module';
import { ParticlesModule } from 'angular-particle';
import { FormsModule } from '@angular/forms';

@NgModule({
  exports: [AccueilComponent],
  declarations: [AccueilComponent ],
  imports: [ 
  CommonModule,
  GuiModule,
  ParticlesModule ,
  FormsModule
  ]
})
export class AccueilModule { }
