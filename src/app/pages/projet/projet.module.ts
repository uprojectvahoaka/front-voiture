import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListeProjetComponent } from './liste-projet/liste-projet.component';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  exports: [ListeProjetComponent],
  declarations: [ListeProjetComponent],
  imports: [
    CommonModule,
    DataTablesModule
  ]
})
export class ProjetModule { }
