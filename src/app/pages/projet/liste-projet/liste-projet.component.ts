import { Component, OnInit } from '@angular/core';
import { ProjetService } from 'src/app/services/projet.service';

@Component({
  selector: 'app-liste-projet',
  templateUrl: './liste-projet.component.html',
  styleUrls: ['./liste-projet.component.css']
})
export class ListeProjetComponent implements OnInit {

  constructor(public projetApi: ProjetService) { }

  ngOnInit() {
    this.projetApi.getProjets().subscribe((listeProjet:any) => {
      console.log(listeProjet);
    });
  }

}
