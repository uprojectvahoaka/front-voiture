import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListeUtilisateurComponent } from './liste-utilisateur/liste-utilisateur.component';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  exports: [ListeUtilisateurComponent, UtilisateurComponent],
  declarations: [ListeUtilisateurComponent, UtilisateurComponent],
  imports: [
    CommonModule,
    DataTablesModule
  ]
})
export class UtilisateurModule { }
