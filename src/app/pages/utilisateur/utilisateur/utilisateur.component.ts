import { Component, OnInit } from '@angular/core';
import { UTILISATEUR } from '../../../interdata/mockUtilisateur';

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.css']
})
export class UtilisateurComponent implements OnInit {

  public listeUtilisateur: any;

  constructor() {
    this.listeUtilisateur = UTILISATEUR;
   }

  ngOnInit() {
  }

}
