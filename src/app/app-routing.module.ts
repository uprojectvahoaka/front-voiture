import { NgModule } from "@angular/core";
import { RouterModule, Routes, ExtraOptions } from "@angular/router";
import { AccueilComponent } from "./pages/accueil/accueil.component";
import { HeroesComponent } from "./pages/heroes/heroes.component";
import { Container1Component } from "./racine/container/container1/container1.component";

const appRoutes: Routes = [
  {
    path: "login",
    loadChildren: "./racine/login/login.module#LoginModule"
  },
  { path: "heroes",
    component: HeroesComponent 
  },
  { path: "accueil",
    component: Container1Component 
  },
  { path: "", redirectTo: "login", pathMatch: "full" }
];

const config: ExtraOptions = {
  useHash: true,
  onSameUrlNavigation: "reload"
};
// { preloadingStrategy: PreloadAllModules }

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
